import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/ChangePassword_respository.dart';
import 'package:noteapplication/state/ChangePassword_state.dart';

class ChangePasswordCubit extends Cubit<ChangePasswordState>{
  final ChangePasswordRespository _repository;

  ChangePasswordCubit(this._repository) : super(InitialChangePasswordState());

  //function get state change password
  Future<void> getChangePassword(String password, String newPassword) async{
    try{
      var result = await _repository.getChangePassword(password, newPassword);
      emit(SuccessLoadChangePasswordState(result));
    }catch(e){
      emit(FailureLoadChangePasswordState(e.toString()));
    }
  }
}