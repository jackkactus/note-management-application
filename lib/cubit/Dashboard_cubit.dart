import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/Dashboard_respository.dart';
import 'package:noteapplication/state/Dashboard_state.dart';

class DashboardCubit extends Cubit<DashboardState>{
  final DashboardRepository _repository;

  DashboardCubit(this._repository) : super(InitialDashboardState());

  //function get state of dashboard
  Future<void> getDashboardInfo(String email) async{
    emit(LoadingDashboardState());
    try{
      var result = await _repository.getDashboard(email);
      emit(SuccessLoadDashboardState(result));
    }catch(e){
      emit(FailureLoadDashboardState(e.toString()));
    }
  }
}