import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/EditProfile_repository.dart';
import 'package:noteapplication/respository/UserSignIn_respository.dart';
import 'package:noteapplication/state/EditProfile_state.dart';

class EditProfileCubit extends Cubit<EditProfileState> {
  final EditProfileRepository _repository;
  final UserRespository _userRepository;

  EditProfileCubit(this._repository, this._userRepository)
      : super(InitialEditProfileState());

  //function get state when update profile
  Future<void> getEditProfile(
      String newEmail, String firstname, String lastname) async {
    try {
      var result =
          await _repository.updateProfile(newEmail, firstname, lastname);
      emit(SuccessLoadEditProfileState(result));
    } catch (e) {
      emit(FailureLoadEditProfileState(e.toString()));
    }
  }

  //function get state of data info in userdata
  Future<void> checkSignIn(String prefsEmail, String prefsPassword) async {
    try {
      var infoUser =
          await _userRepository.signInUser(prefsEmail, prefsPassword);
      emit(LoadingEditProfileState(infoUser));
    } catch (e) {
      emit(FailureCheckSignInState(e.toString()));
    }
  }
}
