import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/UserSignIn_respository.dart';
import 'package:noteapplication/state/UserSignIn_state.dart';



class UserSignInCubit extends Cubit<UserSignInState> {
  final UserRespository _repository;

  UserSignInCubit(this._repository) : super(InitialSignInState());

  //function get state sign in user
  Future<void> signIn(String email, String password) async {
    emit(LoadingSignInState());
    try {
      var result = await _repository.signInUser(email, password);
      emit(SuccessSignInState(result));
    } catch (e) {
      emit(FailSignInState(e.toString()));
    }
  }
}
