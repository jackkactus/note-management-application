import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/UserSignUp_respository.dart';
import 'package:noteapplication/state/UserSignUp_state.dart';

class UserSignUpCubit extends Cubit<UserSignUpState> {
  final SignUpRespository _repository;

  UserSignUpCubit(this._repository) : super(InitialUserState());

  //function get state sign up user
  Future<void> createProfile(String email, String pass) async {
    emit(LoadingUserState());
    try {
      var result = await _repository.signUpUser(email, pass);
      emit(SuccessSignUpState(result));
    } catch (e) {
      emit(FailureSignUpState(e.toString()));
    }
  }
}
