import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/category_repository.dart';
import 'package:noteapplication/state/category_state.dart';

class CategoryCubit extends Cubit<CategoryState> {
  final CategoryRepository _repository;

  CategoryCubit(this._repository):super(InitialCategoryState());

  //state get Category
  Future<void> getAllCategory(String email) async {
    emit(LoadingCategoryState());
    try{
      var result = await _repository.getAllCategory(email);
      emit(SuccessLoadAllCategoryState(result));
    } catch(e){
      emit(FailureCategoryState(e.toString()));
    }
  }

  //state create category
  Future<void> createCategory(String email, String name) async {
    emit(LoadingCategoryState());
    try{
      var result = await _repository.createCategory(email, name);
      emit(SuccessSubmitCategoryState(result));
    } catch(e){
      emit(FailureCategoryState(e.toString()));
    }
  }

  //state update category
  Future<void> updateCategory(String email, String oldName, String newName) async {
    emit(LoadingCategoryState());
    try{
      var result = await _repository.updateCategory(email, oldName, newName);
      emit(SuccessUpdateCategoryState(result));
    } catch(e){
      emit(FailureCategoryState(e.toString()));
    }
  }

  //state delete category
  Future<void> deleteCategory(String email, String name) async {
    emit(LoadingCategoryState());
    try{
      var result = await _repository.deleteCategory(email, name);
      emit(SuccessLoadAllCategoryState(result));
    } catch(e){
      emit(FailureCategoryState(e.toString()));
    }
  }
}