import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/note_repository.dart';
import 'package:noteapplication/state/note_state.dart';

import '../model/note_model.dart';

class NoteCubit extends Cubit<NoteState> {
  final NoteRepository _repository;

  NoteCubit(this._repository):super(InitialNoteState());

  Future<void> getAllNote(String email) async {
    emit(LoadingNoteState());
    try{
      var result = await _repository.getAllNote(email);
      emit(SuccessLoadAllNoteState(result));
    } catch(e){
      emit(FailureNoteState(e.toString()));
    }
  }

  Future<void> createNote(NoteData noteData) async {
    emit(LoadingNoteState());
    try{
      var result = await _repository.createNote(noteData);
      emit(SuccessSubmitNoteState(result));
    } catch(e){
      emit(FailureNoteState(e.toString()));
    }
  }

  Future<void> updateNote(String oldName, NoteData noteData) async {
    emit(LoadingNoteState());
    try{
      var result = await _repository.updateNote(
          oldName, noteData);
      emit(SuccessUpdateNoteState(result));
    } catch(e){
      emit(FailureNoteState(e.toString()));
    }
  }

  Future<void> deleteNote(String email, String name) async {
    emit(LoadingNoteState());
    try{
      var result = await _repository.deleteNote(email, name);
      emit(SuccessLoadAllNoteState(result));
    } catch(e){
      emit(FailureNoteState(e.toString()));
    }
  }
}