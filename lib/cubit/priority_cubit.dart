import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/priority_repository.dart';
import 'package:noteapplication/state/priority_state.dart';
class PriorityCubit extends Cubit<PriorityState> {
  final PriorityRepository _repository;

  PriorityCubit(this._repository):super(InitialPriorityState());

  Future<void> getAllPriority(String email) async {
    emit(LoadingPriorityState());
    try{
      var result = await _repository.getAllPriority(email);
      emit(SuccessLoadAllPriorityState(result));
    } catch(e){
      emit(FailurePriorityState(e.toString()));
    }
  }

  Future<void> createPriority(String email, String name) async {
    emit(LoadingPriorityState());
    try{
      var result = await _repository.createPriority(email, name);
      emit(SuccessSubmitPriorityState(result));
    } catch(e){
      emit(FailurePriorityState(e.toString()));
    }
  }

  Future<void> updatePriority(String email, String oldName, String newName) async {
    emit(LoadingPriorityState());
    try{
      var result = await _repository.updatePriority(email, oldName, newName);
      emit(SuccessUpdatePriorityState(result));
    } catch(e){
      emit(FailurePriorityState(e.toString()));
    }
  }

  Future<void> deletePriority(String email, String name) async {
    emit(LoadingPriorityState());
    try{
      var result = await _repository.deletePriority(email, name);
      emit(SuccessLoadAllPriorityState(result));
    } catch(e){
      emit(FailurePriorityState(e.toString()));
    }
  }
}