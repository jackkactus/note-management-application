import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/respository/status_repository.dart';
import 'package:noteapplication/state/status_state.dart';

class StatusCubit extends Cubit<StatusState> {
  final StatusRepository _repository;

  StatusCubit(this._repository):super(InitialStatusState());

  Future<void> getAllStatus(String email) async {
    emit(LoadingStatusState());
    try{
      var result = await _repository.getAllStatus(email);
      emit(SuccessLoadAllStatusState(result));
    } catch(e){
      emit(FailureStatusState(e.toString()));
    }
  }

  Future<void> createStatus(String email, String name) async {
    emit(LoadingStatusState());
    try{
      var result = await _repository.createStatus(email, name);
      emit(SuccessSubmitStatusState(result));
    } catch(e){
      emit(FailureStatusState(e.toString()));
    }
  }

  Future<void> updateStatus(String email, String oldName, String newName) async {
    emit(LoadingStatusState());
    try{
      var result = await _repository.updateStatus(email, oldName, newName);
      emit(SuccessUpdateStatusState(result));
    } catch(e){
      emit(FailureStatusState(e.toString()));
    }
  }

  Future<void> deleteStatus(String email, String name) async {
    emit(LoadingStatusState());
    try{
      var result = await _repository.deleteStatus(email, name);
      emit(SuccessLoadAllStatusState(result));
    } catch(e){
      emit(FailureStatusState(e.toString()));
    }
  }
}