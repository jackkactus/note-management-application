class ChangePassword{
  final int status;
  final String? error;

  ChangePassword({required this.status, this.error});

  factory ChangePassword.fromJson(Map<String, dynamic> json){
    return ChangePassword(status: json['status'], error: json['error']);
  }
}