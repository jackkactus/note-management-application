class Dashboard {
  final int status;
  final List<dynamic> data;

  const Dashboard({required this.status, required this.data});

  factory Dashboard.fromJson(Map<String, dynamic> json) {
    return Dashboard(status: json['status'], data: json['data']);
  }
}
