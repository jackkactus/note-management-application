class EditProfile {
  final int status;

  EditProfile({required this.status});

  factory EditProfile.fromJson(Map<String, dynamic> json) {
    return EditProfile(status: json['status']);
  }
}
