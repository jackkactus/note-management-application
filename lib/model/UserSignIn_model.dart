class UserSignIn{
  final int? status;
  final int? error;
  final InfoUser? info;

  UserSignIn({required this.status, this.error, this.info});

  factory UserSignIn.fromJson(Map<String, dynamic> json){
    return UserSignIn(
      status: json['status'],
      error: json['error'],
      info: InfoUser.fromJson(json['info']),
    );
  }
}

class InfoUser{
  final String? firstname;
  final String? lastname;

  InfoUser({ this.firstname,  this.lastname});

  factory InfoUser.fromJson(Map<String, dynamic> json){
    return InfoUser(
        firstname: json['FirstName'],
        lastname: json['LastName']
    );
  }
}