class UserSignUp{
  final int? status;
  final int? error;

  UserSignUp({this.status, this.error});

  factory UserSignUp.fromJson(Map<String, dynamic> json){
    return UserSignUp(
      status: json['status'],
      error: json['error'],
    );
  }

}