class Category {
  List<dynamic>? data;
  int? status;

  Category({this.status, this.data});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
        status: json['status'],
        data : json['data']
    );
  }
}
