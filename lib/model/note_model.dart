class Note {
  final List<dynamic>? data;
  final int? status;

  const Note({required this.status, required this.data});

  factory Note.fromJson(Map<String, dynamic> json) {
    return Note(
        status: json['status'],
        data : json['data']
    );
  }
}

class NoteData {
  String? name;
  String? priority;
  String? category;
  String? status;
  String? planDate;
  String? email;

  NoteData({this.name, this.priority, this.category, this.status, this.planDate, this.email});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic> {
      'name': name,
      'priority': priority,
      'category': category,
      'status': status,
      'planDate': planDate,
      'email': email,
    };

    return map;
  }

}