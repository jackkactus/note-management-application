class Priority {
  List<dynamic>? data;
  int? status;

  Priority({this.status, this.data});

  factory Priority.fromJson(Map<String, dynamic> json) {
    return Priority(
        status: json['status'],
        data : json['data']
    );
  }
}
