class Status {
  List<dynamic>? data;
  int? status;

  Status({required this.status, required this.data});

  factory Status.fromJson(Map<String, dynamic> json) {
    return Status(
        status: json['status'],
        data : json['data']
    );
  }
}
