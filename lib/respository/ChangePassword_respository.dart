import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/ChangePassword_model.dart';
import 'package:noteapplication/util/constance.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePasswordRespository {

  //function change password
  Future<ChangePassword> getChangePassword(String password, String newPassword) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefsEmail = prefs.getString(Constance.getEmail) ?? "";

    final String url =
        '${Constance.baseUrl}update?tab=Profile&email=$prefsEmail&pass=$password&npass=$newPassword';
    final Uri uri = Uri.parse(url);
    final response = await http.get(uri);

    if (response.statusCode == Constance.statusCode200) {
      return ChangePassword.fromJson(jsonDecode(response.body));
    }
    throw Exception('Failed to Change Password');
  }
}
