import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/Dashboard_Model.dart';
import 'package:noteapplication/util/constance.dart';

class DashboardRepository{

  //function get Dashboard data
  Future<Dashboard> getDashboard(String prefsEmail) async{
    final String url = '${Constance.baseUrl}get?tab=Dashboard&email=$prefsEmail';
    final Uri uri = Uri.parse(url);

    final response = await http.get(uri);
    if(response.statusCode == Constance.statusCode200){
      return Dashboard.fromJson(jsonDecode(response.body));
    }
    throw Exception('Failed to dashboard');
  }
}