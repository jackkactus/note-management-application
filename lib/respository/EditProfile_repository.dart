import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/EditProfile_model.dart';
import 'package:noteapplication/model/ObjectUpdateProfile.dart';
import 'package:noteapplication/util/constance.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfileRepository {
  //function update profile user
  Future<EditProfile> updateProfile(String newEmail, String firstname, String lastname) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefsEmail = prefs.getString(Constance.getEmail) ?? "";
    final String url =
        '${Constance.baseUrl}update?tab=Profile&email=$prefsEmail'
        '&nemail=$newEmail&firstname=$firstname'
        '&lastname=$lastname';
    final Uri uri = Uri.parse(url);

    final response = await http.get(uri);
    if (response.statusCode == Constance.statusCode200) {
      return EditProfile.fromJson(jsonDecode(response.body));
    }
    throw Exception('Failed to Edit Profile');
  }
}
