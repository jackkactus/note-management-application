import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/UserSignIn_model.dart';
import 'package:noteapplication/util/constance.dart';

class UserRespository {
  //function sign in user
  Future<UserSignIn> signInUser(String email, String password) async {
    final url = "${Constance.baseUrl}login?email=$email&pass=$password";
    final uri = Uri.parse(url);

    final response = await http.get(uri);
    if (response.statusCode == Constance.statusCode200) {
      return UserSignIn.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to sign in ${response.statusCode}');
    }
  }

}