import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/UserSignUp_model.dart';
import 'package:noteapplication/util/constance.dart';

class SignUpRespository {
  //function sign up user
  Future<UserSignUp> signUpUser(String email, String password) async {
    final url = '${Constance.baseUrl}signup?email=$email&pass=$password'
        '&firstname=" "&lastname=" "';
    final uri = Uri.parse(url);

    final response = await http.get(uri);
    if (response.statusCode == Constance.statusCode200) {
      return UserSignUp.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to create profile ${response.statusCode}');
    }
  }

}