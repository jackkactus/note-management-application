import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/category_model.dart';
import 'package:noteapplication/util/constance.dart';


class CategoryRepository {
  Future<Category> getAllCategory(String email) async {
    String url = '${Constance.baseUrl}get?tab=Category&email=$email';
    final response = await http.get(Uri.parse(url));

    if(response.statusCode == 200){
      return Category.fromJson(jsonDecode(response.body));
    }
    throw Exception('Failed to load Category data from ${response.statusCode}');
  }

  Future<Category> createCategory(String email, String name) async {
    String url = '${Constance.baseUrl}add?tab=Category&email=$email&name=$name';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Category.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to create Category ${response.statusCode}');
    }
  }

  Future<Category> updateCategory(String email, String oldName, String newName) async {
    String url = '${Constance.baseUrl}update?tab=Category&email=$email&name=$oldName&nname=$newName';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Category.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to update Category ${response.statusCode}');
    }
  }

  Future<Category> deleteCategory(String email, String name) async {
    String url = '${Constance.baseUrl}del?tab=Category&email=$email&name=$name';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Category.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to delete Category ${response.statusCode}');
    }
  }
}