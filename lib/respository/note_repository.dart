import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/note_model.dart';
import 'package:noteapplication/util/constance.dart';

class NoteRepository{
  Future<Note> getAllNote(String email) async {
    String url = '${Constance.baseUrl}get?tab=Note&email=$email';
    final response = await http.get(Uri.parse(url));

    if(response.statusCode == 200){
      return Note.fromJson(jsonDecode(response.body));
    }
    throw Exception('Failed to load Note data from ${response.statusCode}');
  }

  Future<Note> createNote(NoteData noteData) async {
    String url = '${Constance.baseUrl}add?tab=Note'
        '&email=${noteData.email}'
        '&name=${noteData.name}'
        '&Priority=${noteData.priority}'
        '&Category=${noteData.category}'
        '&Status=${noteData.status}'
        '&PlanDate=${noteData.planDate}';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Note.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to create Note ${response.statusCode}');
    }
  }

  Future<Note> updateNote(String oldName, NoteData noteData) async {
    String url = '${Constance.baseUrl}update?tab=Note'
        '&email=${noteData.email}'
        '&name=$oldName'
        '&nname=${noteData.name}'
        '&Priority=${noteData.priority}'
        '&Category=${noteData.category}'
        '&Status=${noteData.status}'
        '&PlanDate=${noteData.planDate}';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Note.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to update Note ${response.statusCode}');
    }
  }

  Future<Note> deleteNote(String email, String name) async {
    String url = '${Constance.baseUrl}del?tab=Note&email=$email&name=$name';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Note.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to delete Note ${response.statusCode}');
    }
  }

}