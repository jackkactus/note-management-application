import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/priority_model.dart';
import 'package:noteapplication/util/constance.dart';


class PriorityRepository {
  Future<Priority> getAllPriority(String email) async {
    String url = '${Constance.baseUrl}get?tab=Priority&email=$email';
    final response = await http.get(Uri.parse(url));

    if(response.statusCode == 200){
      return Priority.fromJson(jsonDecode(response.body));
    }
    throw Exception('Failed to load Priority data from ${response.statusCode}');
  }

  Future<Priority> createPriority(String email, String name) async {
    String url = '${Constance.baseUrl}add?tab=Priority&email=$email&name=$name';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Priority.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to create Priority ${response.statusCode}');
    }
  }

  Future<Priority> updatePriority(String email, String oldName, String newName) async {
    String url = '${Constance.baseUrl}update?tab=Priority&email=$email&name=$oldName&nname=$newName';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Priority.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to update Priority ${response.statusCode}');
    }
  }

  Future<Priority> deletePriority(String email, String name) async {
    String url = '${Constance.baseUrl}del?tab=Priority&email=$email&name=$name';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Priority.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to delete Priority ${response.statusCode}');
    }
  }
}