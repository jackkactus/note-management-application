import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:noteapplication/model/status_model.dart';
import 'package:noteapplication/util/constance.dart';


class StatusRepository {
  Future<Status> getAllStatus(String email) async {
    String url = '${Constance.baseUrl}get?tab=Status&email=$email';
    final response = await http.get(Uri.parse(url));

    if(response.statusCode == 200){
      return Status.fromJson(jsonDecode(response.body));
    }
    throw Exception('Failed to load Status data from ${response.statusCode}');
  }

  Future<Status> createStatus(String email, String name) async {
    String url = '${Constance.baseUrl}add?tab=Status&email=$email&name=$name';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Status.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to create Status ${response.statusCode}');
    }
  }

  Future<Status> updateStatus(String email, String oldName, String newName) async {
    String url = '${Constance.baseUrl}update?tab=Status&email=$email&name=$oldName&nname=$newName';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Status.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to update Status ${response.statusCode}');
    }
  }

  Future<Status> deleteStatus(String email, String name) async {
    String url = '${Constance.baseUrl}del?tab=Status&email=$email&name=$name';
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return Status.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to delete Status ${response.statusCode}');
    }
  }
}