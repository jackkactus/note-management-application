import 'package:noteapplication/model/ChangePassword_model.dart';
import 'package:noteapplication/state/EditProfile_state.dart';

abstract class ChangePasswordState{}

class InitialChangePasswordState extends ChangePasswordState{}

//sate successful load change password page
class SuccessLoadChangePasswordState extends ChangePasswordState{
  final ChangePassword changePassword;
  SuccessLoadChangePasswordState(this.changePassword);
}

//state fail load change password page
class FailureLoadChangePasswordState extends ChangePasswordState{
  final String message;
  FailureLoadChangePasswordState(this.message);
}