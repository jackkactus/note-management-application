import 'package:noteapplication/model/Dashboard_Model.dart';

abstract class DashboardState{}

class InitialDashboardState extends DashboardState{}
class LoadingDashboardState extends DashboardState{}

//state success load dashboard page
class SuccessLoadDashboardState extends DashboardState{
  final Dashboard dashboard;
  SuccessLoadDashboardState(this.dashboard);
}

//state fail load dashboard page
class FailureLoadDashboardState extends DashboardState{
  final String message;
  FailureLoadDashboardState(this.message);
}