import 'package:noteapplication/model/EditProfile_model.dart';
import 'package:noteapplication/model/UserSignIn_model.dart';

abstract class EditProfileState{}

class InitialEditProfileState extends EditProfileState{}

// state loading edit profile page
class LoadingEditProfileState extends EditProfileState{
  final UserSignIn userSignIn;
  LoadingEditProfileState(this.userSignIn);
}

//state fail sign in
class FailureCheckSignInState extends EditProfileState{
  final String message;
  FailureCheckSignInState(this.message);
}

//state success update profile
class SuccessLoadEditProfileState extends EditProfileState{
  final EditProfile editProfile;
  SuccessLoadEditProfileState(this.editProfile);
}

//state fail update profile
class FailureLoadEditProfileState extends EditProfileState{
  final String message;
  FailureLoadEditProfileState(this.message);
}


