
import 'package:noteapplication/model/UserSignIn_model.dart';

abstract class UserSignInState{}

class InitialSignInState extends UserSignInState{}

class LoadingSignInState extends UserSignInState{}

//state fail sign in user
class FailSignInState extends UserSignInState{
  final String mess;
  FailSignInState(this.mess);
}

//state success sign in user
class SuccessSignInState extends UserSignInState{
  final UserSignIn userSignIn;
  SuccessSignInState(this.userSignIn);
}

