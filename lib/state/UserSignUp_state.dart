import 'package:noteapplication/model/UserSignUp_model.dart';

abstract class UserSignUpState{}

class InitialUserState extends UserSignUpState{}

class LoadingUserState extends UserSignUpState{}

//state fail sign up user
class FailureSignUpState extends UserSignUpState{
  final String errorMessage;
  FailureSignUpState(this.errorMessage);
}

//state success sign up user
class SuccessSignUpState extends UserSignUpState{
  final UserSignUp userSignUp;
  SuccessSignUpState(this.userSignUp);
}