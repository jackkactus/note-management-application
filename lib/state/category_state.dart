

import 'package:noteapplication/model/category_model.dart';

abstract class CategoryState{}

class InitialCategoryState extends CategoryState{}

class LoadingCategoryState extends CategoryState{}

class FailureCategoryState extends CategoryState{
  final String errorMessage;
  FailureCategoryState(this.errorMessage);
}

class SuccessLoadAllCategoryState extends CategoryState{
  final Category listCategory;
  SuccessLoadAllCategoryState(this.listCategory);
}

class SuccessSubmitCategoryState extends CategoryState{
  final Category category;
  SuccessSubmitCategoryState(this.category);
}

class SuccessUpdateCategoryState extends CategoryState{
  final Category category;
  SuccessUpdateCategoryState(this.category);
}
