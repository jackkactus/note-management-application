

import 'package:noteapplication/model/note_model.dart';

abstract class NoteState{}

class InitialNoteState extends NoteState{}

class LoadingNoteState extends NoteState{}

class FailureNoteState extends NoteState{
  final String errorMessage;
  FailureNoteState(this.errorMessage);
}

class SuccessLoadAllNoteState extends NoteState{
  final Note listNote;
  SuccessLoadAllNoteState(this.listNote);
}

class SuccessSubmitNoteState extends NoteState{
  final Note note;
  SuccessSubmitNoteState(this.note);
}

class SuccessUpdateNoteState extends NoteState{
  final Note note;
  SuccessUpdateNoteState(this.note);
}
