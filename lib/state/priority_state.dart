

import 'package:noteapplication/model/priority_model.dart';

abstract class PriorityState{}

class InitialPriorityState extends PriorityState{}

class LoadingPriorityState extends PriorityState{}

class FailurePriorityState extends PriorityState{
  final String errorMessage;
  FailurePriorityState(this.errorMessage);
}

class SuccessLoadAllPriorityState extends PriorityState{
  final Priority listPriority;
  SuccessLoadAllPriorityState(this.listPriority);
}

class SuccessSubmitPriorityState extends PriorityState{
  final Priority priority;
  SuccessSubmitPriorityState(this.priority);
}

class SuccessUpdatePriorityState extends PriorityState{
  final Priority priority;
  SuccessUpdatePriorityState(this.priority);
}
