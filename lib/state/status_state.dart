

import 'package:noteapplication/model/status_model.dart';

abstract class StatusState{}

class InitialStatusState extends StatusState{}

class LoadingStatusState extends StatusState{}

class FailureStatusState extends StatusState{
  final String errorMessage;
  FailureStatusState(this.errorMessage);
}

class SuccessLoadAllStatusState extends StatusState{
  final Status listStatus;
  SuccessLoadAllStatusState(this.listStatus);
}

class SuccessSubmitStatusState extends StatusState{
  final Status status;
  SuccessSubmitStatusState(this.status);
}

class SuccessUpdateStatusState extends StatusState{
  final Status status;
  SuccessUpdateStatusState(this.status);
}
