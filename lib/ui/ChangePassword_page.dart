import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/cubit/ChangePassword_cubit.dart';
import 'package:noteapplication/respository/ChangePassword_respository.dart';
import 'package:noteapplication/state/ChangePassword_state.dart';
import 'package:noteapplication/ui/Dashboard_page.dart';
import 'package:noteapplication/ui/SignInForm.dart';
import 'package:noteapplication/util/constance.dart';

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: _HomePage(),
    );
  }
}

class _HomePage extends StatefulWidget {
  const _HomePage();

  @override
  State<_HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<_HomePage> {
  final _changePasswordCubit = ChangePasswordCubit(ChangePasswordRespository());
  final _formKey = GlobalKey<FormState>();
  String? message = "error";

  final currentPasswordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Change Password',
          style:
              TextStyle(color: Colors.limeAccent, fontWeight: FontWeight.w400),
        ),
        backgroundColor: Colors.teal,
      ),
      body: Center(
        child: BlocProvider.value(
          value: _changePasswordCubit,
          child: BlocListener<ChangePasswordCubit, ChangePasswordState>(
            listener: (_, state) {
              if (state is InitialChangePasswordState) {
                const CircularProgressIndicator();
              } else if (state is SuccessLoadChangePasswordState) {
                final passwordData = state.changePassword;
                final status = passwordData.status;
                if (status == Constance.success) {
                  message = "Change password successful";
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => const SignInUser()));
                } else if (status == Constance.failChangePassword) {
                  message = 'Password fail';
                }
                setState(() {
                  currentPasswordController.text = "";
                  newPasswordController.text = "";
                  confirmPasswordController.text = "";
                });
                ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text(message!)));
              }
            },
            child: formChangePassword(),
          ),
        ),
      ),
    );
  }

  Widget formChangePassword() => Center(
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.85,
          height: MediaQuery.of(context).size.height * 0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Form(
                      key: _formKey,
                      child: SingleChildScrollView(
                          child: Column(
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            child: const Text(
                              'Change Your Password',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.green,
                              ),
                            ),
                          ),
                          const Padding(padding: EdgeInsets.only(bottom: 20)),
                          TextFormField(
                            obscureText: true, //hide pass
                            obscuringCharacter: "*",
                            decoration: const InputDecoration(
                                hintText: 'Enter current password',
                                contentPadding: EdgeInsets.only(left: 25)),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter your current password';
                              }
                              return null;
                            },
                            controller: currentPasswordController,
                          ),
                          TextFormField(
                            obscureText: true, //hide pass
                            obscuringCharacter: "*",
                            decoration: const InputDecoration(
                                hintText: 'Enter your new password',
                                contentPadding: EdgeInsets.only(left: 25)),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter your password';
                              } else if (value.length < 8) {
                                return "Length of password's character"
                                    " must be 8 or greater";
                              }
                              return null;
                            },
                            controller: newPasswordController,
                          ),
                          TextFormField(
                            obscureText: true, //hide pass
                            obscuringCharacter: "*",
                            decoration: const InputDecoration(
                                hintText: 'Enter password again',
                                contentPadding: EdgeInsets.only(left: 25)),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please confirm your password';
                              } else if (value != newPasswordController.text) {
                                return "Password miss";
                              }
                              return null;
                            },
                            controller: confirmPasswordController,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: () async {
                                  String password =
                                      currentPasswordController.text;
                                  String newPassword =
                                      newPasswordController.text;
                                  if (_formKey.currentState!.validate() &&
                                      newPasswordController.text ==
                                          confirmPasswordController.text) {
                                    _changePasswordCubit.getChangePassword(
                                        password, newPassword);
                                  }
                                },
                                child: const Text('Change'),
                              ),
                              const Padding(
                                  padding: EdgeInsets.only(
                                      top: 150, left: 40, right: 40)),
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const DashboardPage()));
                                  },
                                  child: const Text('Home'))
                            ],
                          ),
                        ],
                      )))),
            ],
          ),
        ),
      );
}
