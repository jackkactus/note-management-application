import 'package:flutter/material.dart';
import 'package:noteapplication/ui/ChangePassword_page.dart';
import 'package:noteapplication/ui/PieChart_Page.dart';
import 'package:noteapplication/ui/SignInForm.dart';
import 'package:noteapplication/ui/category_page.dart';
import 'package:noteapplication/ui/note_page.dart';
import 'package:noteapplication/ui/priority_page.dart';
import 'package:noteapplication/ui/status_page.dart';
import 'package:noteapplication/util/constance.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'EditProfile_page.dart';

void main() {
  runApp(const DashboardPage());
}

class DashboardPage extends StatelessWidget {
  const DashboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: _HomePage(),
    );
  }
}

class _HomePage extends StatefulWidget {
  const _HomePage({super.key});

  @override
  State<_HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<_HomePage> {
  var currentPage = DrawerSections.home;
  String updateTitle = 'Dashboard';
  String name = "Your Name";
  String email = "Your Email";

  void _clearReference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefEmail = prefs.getString(Constance.getEmail) ?? "";
    final prefName = prefs.getString(Constance.getName) ?? "";
    email = prefEmail;
    name = prefName;
  }

  @override
  void initState() {
    super.initState();
    initValue();
  }

  @override
  Widget build(BuildContext context) {
    var container;
    if (currentPage == DrawerSections.home) {
      container = const PieChartPage();
    } else if (currentPage == DrawerSections.category) {
      container = const CategoryPage();
    } else if (currentPage == DrawerSections.priority) {
      container = const PriorityPage();
    } else if (currentPage == DrawerSections.status) {
      container = const StatusPage();
    } else if (currentPage == DrawerSections.note) {
      container = const NotePage();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(
          updateTitle,
          style: const TextStyle(
              color: Colors.limeAccent, fontWeight: FontWeight.w400),
        ),
        backgroundColor: Colors.teal,
      ),
      body: container,
      drawer: Drawer(
        child: SingleChildScrollView(
          child: Column(
            children: [
              UserAccountsDrawerHeader(
                accountName: Text(name),
                accountEmail: Text(email),
                currentAccountPicture: const CircleAvatar(
                  child: ClipOval(
                    child: Image(image: AssetImage('images/husky.jpg')),
                  ),
                ),
              ),
              myDrawerList(),
            ],
          ),
        ),
      ),
    );
  }

  Widget myDrawerList() {
    return Container(
      padding: const EdgeInsets.only(
        top: 10,
      ),
      child: Column(
        // shows the list of menu drawer
        children: [
          menuItem(1, "Home", Icons.camera_alt,
              currentPage == DrawerSections.home ? true : false),
          menuItem(2, "Category", Icons.collections,
              currentPage == DrawerSections.category ? true : false),
          menuItem(3, "Priority", Icons.video_collection,
              currentPage == DrawerSections.priority ? true : false),
          menuItem(4, "Status", Icons.build,
              currentPage == DrawerSections.status ? true : false),
          menuItem(5, "Note", Icons.build,
              currentPage == DrawerSections.note ? true : false),
          const Divider(),
          const Text(
            'Account',
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Colors.black26,
            ),
          ),
          menuItem(6, "Edit Profile", Icons.share,
              currentPage == DrawerSections.editProfile ? true : false),
          menuItem(7, "Change Password", Icons.send,
              currentPage == DrawerSections.changePassword ? true : false),
          menuItem(8, "Logout", Icons.logout,
              currentPage == DrawerSections.logout ? true : false)
        ],
      ),
    );
  }

  Widget menuItem(int id, String title, IconData icon, bool selected) {
    return Material(
      color: selected ? Colors.tealAccent : Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
          setState(() {
            if (id == Constance.pageHome) {
              currentPage = DrawerSections.home;
              updateTitle = 'Dashboard';
            } else if (id == Constance.pageCategory) {
              currentPage = DrawerSections.category;
              updateTitle = 'Category';
            } else if (id == Constance.pagePriority) {
              currentPage = DrawerSections.priority;
              updateTitle = 'Priority';
            } else if (id == Constance.pageStatus) {
              currentPage = DrawerSections.status;
              updateTitle = 'Status';
            } else if (id == Constance.pageNote) {
              currentPage = DrawerSections.note;
              updateTitle = 'Note';
            } else if (id == Constance.pageEditProfile) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (_) => const EditProfilePage()));
              // currentPage = DrawerSections.editProfile;
              // updateTitle = 'Edit Profile';
            } else if (id == Constance.pageChangePassword) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (_) => const ChangePasswordPage()));
              // currentPage = DrawerSections.changePassword;
              // updateTitle = 'Change Password';
            } else if (id == Constance.pageLogout) {
              currentPage == DrawerSections.logout;
              Navigator.push(context,
                  MaterialPageRoute(builder: (_) => const SignInUser()));
              _clearReference();
            }
          });
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 15.0, left: 0, bottom: 15),
          child: Row(
            children: [
              Expanded(
                child: Icon(
                  icon,
                  size: 20,
                  color: Colors.teal,
                ),
              ),
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: const TextStyle(
                    color: Colors.teal,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum DrawerSections {
  home,
  category,
  priority,
  status,
  note,
  editProfile,
  changePassword,
  logout,
}
