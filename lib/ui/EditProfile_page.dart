import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/cubit/EditProfile_cubit.dart';
import 'package:noteapplication/respository/EditProfile_repository.dart';
import 'package:noteapplication/respository/UserSignIn_respository.dart';
import 'package:noteapplication/state/EditProfile_state.dart';
import 'package:noteapplication/ui/Dashboard_page.dart';
import 'package:noteapplication/util/constance.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfilePage extends StatelessWidget {
  const EditProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      // debugShowCheckedModeBanner: false,
      home: _HomePage(),
    );
  }
}

class _HomePage extends StatefulWidget {
  const _HomePage({super.key});

  @override
  State<_HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<_HomePage> {
  final _editProfileCubit =
      EditProfileCubit(EditProfileRepository(), UserRespository());

  final _formKey = GlobalKey<FormState>();

  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();

  void setUserName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constance.getName, lastNameController.text);
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefEmail = prefs.getString(Constance.getEmail) ?? "";
    final prefPassword = prefs.getString(Constance.getPassword) ?? "";
    _editProfileCubit.checkSignIn(prefEmail, prefPassword);
    setState(() {
      emailController.text = prefEmail;
    });
  }

  @override
  void initState() {
    super.initState();
    initValue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Profile',
          style: TextStyle(color: Colors.limeAccent, fontWeight: FontWeight.w400),),
        backgroundColor: Colors.teal,
      ),
      body: Center(
        child: BlocProvider.value(
          value: _editProfileCubit,
          child: BlocListener<EditProfileCubit, EditProfileState>(
            listener: (_, state) {
              if (state is InitialEditProfileState) {
                const CircularProgressIndicator();
              }
              if (state is LoadingEditProfileState) {
                final userData = state.userSignIn;
                final infoUser = userData.info;
                String? firstname = infoUser!.firstname;
                String? lastname = infoUser.lastname;

                setState(() {
                  firstNameController.text = firstname!;
                  lastNameController.text = lastname!;
                });
              }
              if (state is SuccessLoadEditProfileState) {
                final profileData = state.editProfile;
                final status = profileData.status;
                if (status == Constance.success) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Update Successful')));
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Update fail')));
                }
              } if (state is FailureLoadEditProfileState) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(const SnackBar(content: Text('Error')));
              }
            },
            child: formEditProfile(),
          ),
        ),
      ),
    );
  }

  Widget formEditProfile() => Center(
      child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.85,
          height: MediaQuery.of(context).size.height * 0.5,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Form(
                      key: _formKey,
                        child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                        child: Expanded(
                            child: Column(
                      //mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          child: const Text(
                            'Edit Profile',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.green,
                            ),
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(bottom: 20)),
                        TextFormField(
                          decoration: const InputDecoration(
                              hintText: 'Enter your firstname',
                              contentPadding: EdgeInsets.only(left: 25)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter your first name';
                            }
                            return null;
                          },
                          controller: firstNameController,
                        ),
                        TextFormField(
                          decoration: const InputDecoration(
                              hintText: 'Enter your last name',
                              contentPadding: EdgeInsets.only(left: 25)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter your last name';
                            }
                            return null;
                          },
                          controller: lastNameController,
                        ),
                        TextFormField(
                          decoration: const InputDecoration(
                              hintText: 'Enter your email',
                              contentPadding: EdgeInsets.only(left: 25)),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Please enter your email';
                            }
                            return null;
                          },
                          controller: emailController,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              onPressed: () async {
                                String firstname = firstNameController.text;
                                String lastname = lastNameController.text;
                                String email = emailController.text;
                                if (_formKey.currentState!.validate()) {
                                  _editProfileCubit.getEditProfile(
                                      email, firstname, lastname);
                                  setUserName();
                                }
                              },
                              child: const Text('Update'),
                            ),
                            const Padding(
                                padding: EdgeInsets.only(
                                    top: 150, left: 40, right: 40)),
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const DashboardPage()));

                                },
                                child: const Text('Home'))
                          ],
                        )
                      ],
                    )))))
              ])));
}
