import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/cubit/Dashboard_cubit.dart';
import 'package:noteapplication/respository/Dashboard_respository.dart';
import 'package:noteapplication/state/Dashboard_state.dart';
import 'package:noteapplication/ui/Dashboard_page.dart';
import 'package:noteapplication/util/constance.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PieChartPage extends StatelessWidget {
  const PieChartPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Pie Chart',
      home: BuildPieChart(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class BuildPieChart extends StatefulWidget {
  const BuildPieChart({super.key});

  @override
  State<BuildPieChart> createState() => _BuildPieChartState();
}

class _BuildPieChartState extends State<BuildPieChart> {
  final _dashboardCubit = DashboardCubit(DashboardRepository());
  var currentPage = DrawerSections.home;
  String updateTitle = 'Dashboard';

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefEmail = prefs.getString(Constance.getEmail) ?? "";
    _dashboardCubit.getDashboardInfo(prefEmail);
  }

  @override
  void initState() {
    super.initState();
    initValue();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocProvider.value(
        value: _dashboardCubit,
        child: BlocBuilder<DashboardCubit, DashboardState>(
          builder: (context, state) {
            if (state is InitialDashboardState ||
                state is LoadingDashboardState) {
              return const CircularProgressIndicator();
            } else if (state is SuccessLoadDashboardState) {
              final dashboard = state.dashboard;
              final data = dashboard.data;
              if (data.isEmpty) {
                return const Center(
                  child: Text(
                    'Not found data',
                  ),
                );
              } else {
                List myList = [];
                for (int i = 0; i < data.length; i++) {
                  final info = data[i];
                  myList.add(ConvertData(noteName: info[0], count: info[1]));
                }
                Map<String, double> dataMap = {};
                for (var ConvertData in myList) {
                  dataMap[ConvertData.noteName] =
                      double.parse(ConvertData.count);
                }
                return Center(
                  child: PieChart(
                    dataMap: dataMap,
                    chartRadius: MediaQuery.of(context).size.width / 1.7,
                    legendOptions: const LegendOptions(
                      legendPosition: LegendPosition.bottom,
                    ),
                    chartValuesOptions: const ChartValuesOptions(
                      showChartValuesInPercentage: true,
                    ),
                  ),
                );
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class ConvertData {
  String noteName;
  String count;

  ConvertData({required this.noteName, required this.count});
}

