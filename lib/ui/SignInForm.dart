import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/cubit/UserSignIn_cubit.dart';
import 'package:noteapplication/respository/UserSignIn_respository.dart';
import 'package:noteapplication/state/UserSignIn_state.dart';
import 'package:noteapplication/ui/Dashboard_page.dart';
import 'package:noteapplication/ui/SignUpForm.dart';
import 'package:noteapplication/util/constance.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignInUser extends StatelessWidget {
  const SignInUser({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SignInForm(),
    );
  }
}

class SignInForm extends StatefulWidget {
  const SignInForm({super.key});

  @override
  SignInFormState createState() => SignInFormState();
// TODO: implement createState

}

class SignInFormState extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();

  final userEmail = TextEditingController();
  final getEmail = TextEditingController();
  final getPassword = TextEditingController();
  final userPassword = TextEditingController();
  final userSignInCubit = UserSignInCubit(UserRespository());
  String message = "message";

  void setUserPass() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constance.getEmail, userEmail.text);
    prefs.setString(Constance.getPassword, userPassword.text);
  }

  void setRemember(bool flag) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (flag) {
      prefs.setString(Constance.email, userEmail.text);
      prefs.setString(Constance.password, userPassword.text);
    } else {
      await prefs.clear();
    }
    prefs.setBool(Constance.remember, flag);
  }

  Future<void> initValue() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      getEmail.text = preferences.getString(Constance.getEmail) ?? "";
      getPassword.text = preferences.getString(Constance.password) ?? "";
      userEmail.text = preferences.getString(Constance.email) ?? "";
      userPassword.text = preferences.getString(Constance.password) ?? "";
      isChecked = preferences.getBool(Constance.remember) ?? false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isSignIn = true;
    initValue();
  }

  bool isChecked = false;
  bool isSignIn = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Note Management System',
            style: TextStyle(
                color: Colors.limeAccent, fontWeight: FontWeight.w400),
          ),
          backgroundColor: Colors.teal,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (_) => const SignUpProfile()));
          },
          backgroundColor: Colors.teal,
          child: const Icon(Icons.add),
        ),
        body: BlocProvider.value(
            value: userSignInCubit,
            child: BlocListener<UserSignInCubit, UserSignInState>(
              listener: (_, state) {
                if (state is InitialSignInState ||
                    state is LoadingSignInState) {
                  const CircularProgressIndicator();
                } else if (state is SuccessSignInState) {
                  final userData = state.userSignIn;
                  final status = userData.status;
                  final error = userData.error;
                  if (status == Constance.success) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text("Sign In Successful")));
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => const DashboardPage()));
                    setRemember(isChecked);
                    setUserPass();
                  } else if (status == -1 && error == 2) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text("Fail Password")));
                  } else {
                    if (status == Constance.fail &&
                        error == Constance.failPassword) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text("Not Found Email")));
                    }
                  }
                } else if (state is FailSignInState) {
                  ScaffoldMessenger.of(context)
                      .showSnackBar(const SnackBar(content: Text("Error")));
                }
              },
              child: signInForm(),
            )));
  }

  Widget signInForm() => Center(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Padding(padding: EdgeInsets.only(left: 160, right: 10)),
                  const Text(
                    "Note Management System",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.teal,
                        fontSize: 24),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      controller: userEmail,
                      decoration: const InputDecoration(
                          hintText: 'Enter your Email',
                          contentPadding: EdgeInsets.only(left: 25)),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email is Required';
                        }

                        if (!RegExp(
                                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                            .hasMatch(value)) {
                          return 'Please enter a valid email Address';
                        }

                        return null;
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                        controller: userPassword,
                        obscureText: true, //hide pass
                        obscuringCharacter: "*",
                        decoration: const InputDecoration(
                            hintText: 'Enter your Password',
                            contentPadding: EdgeInsets.only(left: 25)),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter your password';
                          } else if (value.length < 8) {
                            return "Length of password's character must be 8 or greater";
                          }
                          return null;
                        }),
                  ),
                  Row(children: [
                    const Padding(padding: EdgeInsets.only(left: 15)),
                    Checkbox(
                      checkColor: Colors.white,
                      value: isChecked,
                      onChanged: (value) {
                        setState(() {
                          isChecked = value!;
                        });
                      },
                    ),
                    const Text('Remember me'),
                  ]),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.teal),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            userSignInCubit.signIn(
                                userEmail.text, userPassword.text);
                          }
                        },
                        child: const Text('Sign In',
                            style: TextStyle(color: Colors.limeAccent)),
                      ),
                      const Padding(
                          padding:
                              EdgeInsets.only(top: 100, left: 160, right: 10)),
                      ElevatedButton(
                          onPressed: () {
                            SystemNavigator.pop();
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.teal),
                          ),
                          child: const Text('Exit',
                              style: TextStyle(color: Colors.limeAccent))),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
