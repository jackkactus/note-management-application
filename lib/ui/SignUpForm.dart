import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:noteapplication/cubit/UserSignUp_cubit.dart';
import 'package:noteapplication/respository/UserSignUp_respository.dart';
import 'package:noteapplication/state/UserSignUp_state.dart';
import 'package:noteapplication/ui/SignInForm.dart';
import 'package:noteapplication/util/constance.dart';

class SignUpProfile extends StatelessWidget{
  const SignUpProfile({super.key});

  @override
  Widget build(BuildContext context){
    return const MaterialApp(
      home: SignUpForm(),
    );
  }
}

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  SignUpFormState createState() => SignUpFormState();
// TODO: implement createState

}

class SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  final _userEmail = TextEditingController();
  final _userPassword = TextEditingController();
  final _userRePassword = TextEditingController();
  final userSignUpCubit = UserSignUpCubit(SignUpRespository());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Note Management System',
            style: TextStyle(
                fontWeight: FontWeight.w400,
                color: Colors.limeAccent,
                fontSize: 20),),
          backgroundColor: Colors.teal,
        ),
        body: BlocProvider.value(
            value: userSignUpCubit,
            child: BlocListener<UserSignUpCubit, UserSignUpState>(
              listener: (_, state) {
                if (state is InitialUserState || state is LoadingUserState) {
                  const CircularProgressIndicator();
                } else if (state is SuccessSignUpState) {
                  final userData = state.userSignUp;
                  final status = userData.status;
                  final error = userData.error;
                  if (status == Constance.success) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Sign up successful')));

                    _userEmail.text = '';
                    _userPassword.text = '';
                    _userRePassword.text = '';
                  } else if (status == Constance.fail && error == Constance.emailExist) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Email exist')));
                  }
                } else if (state is FailureSignUpState) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Sign up fail')));
                }
              },
              child: _signUpForm(),
            )));
  }

  Widget _signUpForm() {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Padding(
                  padding: EdgeInsets.only(top: 80, left: 160, right: 10)),
              const Text(
                "Sign Up Form",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.teal,
                    fontSize: 24),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                margin: const EdgeInsets.only(top: 20),
                child: TextFormField(
                  controller: _userEmail,
                  decoration: const InputDecoration(
                      hintText: 'Enter your Email',
                      contentPadding: EdgeInsets.only(left: 25)),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Email is Required';
                    }

                    if (!RegExp(
                            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                        .hasMatch(value)) {
                      return 'Please enter a valid email Address';
                    }

                    return null;
                  },
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                margin: const EdgeInsets.only(top: 20),
                child: TextFormField(
                    controller: _userPassword,
                    obscureText: true,
                    //hide pass
                    obscuringCharacter: "*",
                    decoration: const InputDecoration(
                        hintText: 'Enter your Password',
                        contentPadding: EdgeInsets.only(left: 25)),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter your password';
                      } else if (value.length < 8) {
                        return "Length of password's character must be 8 or greater";
                      }
                      return null;
                    }),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                margin: const EdgeInsets.only(top: 20),
                child: TextFormField(
                    obscureText: true,
                    //hide pass
                    obscuringCharacter: "*",
                    controller: _userRePassword,
                    decoration: const InputDecoration(
                        hintText: 'Enter your Password',
                        contentPadding: EdgeInsets.only(left: 25)),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please confirm your password';
                      } else if (value != _userPassword.text) {
                        return "Password miss";
                      }
                      return null;
                    }),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.teal)),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          userSignUpCubit.createProfile(
                              _userEmail.text, _userPassword.text);
                        }
                      },
                      child: const Text('Sign Up',
                          style: TextStyle(color: Colors.limeAccent))),
                  const Padding(
                      padding: EdgeInsets.only(top: 100, left: 160, right: 10)),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => const SignInUser()));
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.teal),
                      ),
                      child: const Text('Sign In',
                          style: TextStyle(color: Colors.limeAccent)))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

}
