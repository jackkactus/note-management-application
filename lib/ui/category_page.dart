import 'package:noteapplication/respository/category_repository.dart';
import 'package:noteapplication/ui/note_widget/note_import.dart';

class CategoryPage extends StatelessWidget {
  const CategoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key,});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final nameController = TextEditingController();
  final _categoryCubit = CategoryCubit(CategoryRepository());

  bool isUpdate = false;
  String oldName = '';
  String emailValue = '';
  int? statusCode = 0;
  int? statusError = 0;

  @override
  void initState(){
    super.initState();
    initValue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider.value(
        value: _categoryCubit,
        child: BlocBuilder<CategoryCubit, CategoryState>(
          builder: (context, state){
            if(state is InitialCategoryState || state is LoadingCategoryState){
              return const Center(child: CircularProgressIndicator());
            }
            else if(state is SuccessLoadAllCategoryState) {
              final category = state.listCategory;
              statusCode = category.status;
              final categoryData = category.data;
              return ListView.builder(
                  itemCount: categoryData?.length,
                  itemBuilder: (context, index) {
                    final category= categoryData?[index];
                    final color = Constance.lightColors[
                    index % Constance.lightColors.length];
                    return Card(
                      color: color,
                      margin: const EdgeInsets.all(15),
                      child: ListTile(
                        title: Text(category[0]),
                        trailing: PopupMenuButton<int>(
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              value: 1,
                              child: IconButton(
                                onPressed: () {
                                  isUpdate = true;
                                  nameController.text = category[0];
                                  oldName = category[0];
                                  _showDialog(context, category);
                                  _categoryCubit.getAllCategory(emailValue);
                                },
                                icon: const Icon(Icons.edit),
                              ),
                            ),
                            PopupMenuItem(
                                value: 2,
                                child: IconButton(
                                    onPressed: () async {
                                      await _categoryCubit.deleteCategory(
                                          emailValue,
                                          category[0]
                                      );
                                      if(statusCode==1){
                                        _categoryCubit.getAllCategory(emailValue);
                                      }
                                      else if(statusCode==-1 && mounted){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                            const SnackBar(
                                                content: Text('Category Used')
                                            )
                                        );
                                      }
                                    },
                                    icon: const Icon(Icons.delete)
                                )
                            ),
                          ],
                        ),
                      ),
                    );
                  }
              );
            }
            else if(state is FailureCategoryState){
              return Center(child: Text(state.errorMessage),);
            }
            return Text(state.toString());
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            isUpdate = false;
            nameController.text = '';
            _showDialog(context, null);
            _categoryCubit.getAllCategory(emailValue);
          },
          label: const Text('Add Category')
      ),
    );
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      emailValue = prefs.getString(Constance.getEmail) ?? "";
      _categoryCubit.getAllCategory(emailValue);
    });
  }

  Future<void> _showDialog (
      BuildContext context, List<dynamic>? category) async{
    return showDialog(
        context: context,
        builder: (context){
          return StatefulBuilder(builder: (context, setState){
            return Dialog(
              insetPadding: EdgeInsets.zero,
              child: Scaffold(
                appBar: AppBar(
                  title: Text(isUpdate ? 'Update Category' : 'Create new Category'),
                  leading: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    icon: const Icon(Icons.close),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () async {
                          if (isUpdate == true) {
                            await _categoryCubit.updateCategory(emailValue, oldName, nameController.text);
                            if(statusCode==Constance.success){
                              _categoryCubit.getAllCategory(emailValue);
                            }
                            else if(statusCode==Constance.failCategory && mounted){
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                  content: Text('Name is null. Please re-enter name.')
                              ));
                            }
                          }

                          if (isUpdate == false) {
                            await _categoryCubit.createCategory(emailValue, nameController.text);
                            if(statusCode==Constance.success){
                              _categoryCubit.getAllCategory(emailValue);
                            }
                            else if(statusCode==Constance.failCategory && mounted){
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                  content: Text('Name is already exists.')
                              ));
                            }
                          }
                        },
                        child: const Text(
                          'Save',
                          style: TextStyle(color: Colors.white),
                        )
                    )
                  ],
                ),
                body: BlocProvider.value(
                  value: _categoryCubit,
                  child: BlocListener<CategoryCubit ,CategoryState>(
                    listener: (_, state){
                      if(state is SuccessSubmitCategoryState){
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text('Category add successfully')
                        ));
                        setState(() {
                          nameController.clear();
                        });
                      }
                      else if(state is SuccessUpdateCategoryState){
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text('Category update successfully')
                        ));
                      }
                      else if (state is FailureCategoryState){
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(state.errorMessage))
                        );
                      }
                    },
                    child: Stack(
                      children: [_buildFormWidget(), _buildLoadingWidget()],
                    ),
                  ),
                ),
              ),
            );
          });
        }
    );
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
  }

  Widget _buildFormWidget(){
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: const InputDecoration(
                hintText: 'Enter Category',
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.people)
            ),
            controller: nameController,
          ),
        ),
      ],
    );
  }

  Widget _buildLoadingWidget(){
    return BlocBuilder<CategoryCubit, CategoryState>(builder: (_, state) {
      if(state is LoadingCategoryState) {
        return const Center(child: CircularProgressIndicator(),);
      } else {
        return Container();
      }
    });
  }
}

