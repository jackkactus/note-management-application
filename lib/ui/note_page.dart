
import 'package:noteapplication/cubit/status_cubit.dart';
import 'package:noteapplication/model/note_model.dart';
import 'package:noteapplication/respository/note_repository.dart';
import 'package:noteapplication/respository/priority_repository.dart';
import 'package:noteapplication/respository/status_repository.dart';
import 'package:noteapplication/state/note_state.dart';
import 'package:noteapplication/state/status_state.dart';
import 'package:noteapplication/ui/note_widget/note_import.dart';

import '../respository/category_repository.dart';


class NotePage extends StatelessWidget {
  const NotePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({
    super.key,
  });

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final nameController = TextEditingController();
  final _dateController = TextEditingController();
  final _noteCubit = NoteCubit(NoteRepository());
  final _categoryCubit = CategoryCubit(CategoryRepository());
  final _priorityCubit = PriorityCubit(PriorityRepository());
  final _statusCubit = StatusCubit(StatusRepository());

  bool isUpdate = false;
  String oldName = '';
  String emailValue = '';
  int? statusCode = 0;
  int? statusError = 0;
  DateTime _date = DateTime.now();

  String? selectedPriorityItem;
  String? selectedCategoryItem;
  String? selectedStatusItem;

  @override
  void initState() {
    super.initState();
    initValue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider.value(
        value: _noteCubit,
        child: BlocBuilder<NoteCubit, NoteState>(
          builder: (context, state) {
            if (state is InitialNoteState || state is LoadingNoteState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is SuccessLoadAllNoteState) {
              final notes = state.listNote;
              statusCode = notes.status;
              final noteData = notes.data;
              return ListView.builder(
                  itemCount: noteData?.length,
                  itemBuilder: (context, index) {
                    final note = noteData?[index];
                    final color = Constance
                        .lightColors[index % Constance.lightColors.length];
                    return Card(
                      color: color,
                      margin: const EdgeInsets.all(15),
                      child: ListTile(
                        title: Text(note[0],
                          style: const TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Category: ${note[1]}'),
                            Text('Priority: ${note[2]}'),
                            Text('Status: ${note[3]}'),
                            Text('Plan Date: ${note[4]}'),
                          ],
                        ),
                        trailing: PopupMenuButton<int>(
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              value: 1,
                              child: IconButton(
                                onPressed: () {
                                  isUpdate = true;
                                  nameController.text = note[0];
                                  selectedCategoryItem = note[1];
                                  selectedPriorityItem = note[2];
                                  selectedStatusItem = note[3];
                                  _dateController.text = note[4];
                                  oldName = note[0];
                                  _showDialog(context, note);
                                  _noteCubit.getAllNote(emailValue);
                                },
                                icon: const Icon(Icons.edit),
                              ),
                            ),
                            PopupMenuItem(
                                value: 2,
                                child: IconButton(
                                    onPressed: () async {
                                      await _noteCubit.deleteNote(
                                          emailValue, note[0]);
                                      if (statusCode == 1) {
                                        _noteCubit
                                            .getAllNote(emailValue);
                                      }
                                    },
                                    icon: const Icon(Icons.delete))),
                          ],
                        ),
                      ),
                    );
                  });
            } else if (state is FailureNoteState) {
              return Center(
                child: Text(state.errorMessage),
              );
            }
            return Text(state.toString());
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            isUpdate = false;
            nameController.text = '';
            _showDialog(context, null);
            _noteCubit.getAllNote(emailValue);
          },
          label: const Text('Add Note')),
    );
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      emailValue = prefs.getString(Constance.getEmail) ?? "";
      _noteCubit.getAllNote(emailValue);
      _categoryCubit.getAllCategory(emailValue);
      _priorityCubit.getAllPriority(emailValue);
      _statusCubit.getAllStatus(emailValue);
    });
  }

  Future<void> _showDialog(BuildContext context, List<dynamic>? note) async {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              insetPadding: EdgeInsets.zero,
              child: Scaffold(
                appBar: AppBar(
                  title: Text(isUpdate ? 'Update Note' : 'Create new Note'),
                  leading: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    icon: const Icon(Icons.close),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () async {
                          if (isUpdate == true) {
                            await _noteCubit.updateNote(
                                oldName,
                                NoteData(
                                    name: nameController.text,
                                    priority: selectedPriorityItem,
                                    category: selectedCategoryItem,
                                    status: selectedStatusItem,
                                    planDate: _dateController.text,
                                    email: emailValue));
                            if (statusCode == Constance.success) {
                              _noteCubit.getAllNote(emailValue);
                            } else if (statusCode == Constance.failNote && mounted) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text(
                                          'Name is null. Please re-enter name.')));
                            }
                          }

                          if (isUpdate == false) {
                            await _noteCubit.createNote(NoteData(
                                name: nameController.text,
                                priority: selectedPriorityItem,
                                category: selectedCategoryItem,
                                status: selectedStatusItem,
                                planDate: _dateController.text,
                                email: emailValue));
                            if (statusCode == Constance.success) {
                              _noteCubit.getAllNote(emailValue);
                            } else if (statusCode == Constance.failNote && mounted) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content:
                                          Text('Name is already exists.')));
                            }
                          }
                        },
                        child: const Text(
                          'Save',
                          style: TextStyle(color: Colors.white),
                        ))
                  ],
                ),
                body: BlocProvider.value(
                  value: _noteCubit,
                  child: BlocListener<NoteCubit, NoteState>(
                    listener: (_, state) {
                      if (state is SuccessSubmitNoteState) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Note add successfully')));
                        setState(() {
                          nameController.clear();
                        });
                      } else if (state is SuccessUpdateNoteState) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Note update successfully')));
                      } else if (state is FailureNoteState) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(state.errorMessage)));
                      }
                    },
                    child: Stack(
                      children: [_buildFormWidget(), _buildLoadingWidget()],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
  }

  Widget _buildFormWidget() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        children: [
          TextFormField(
            decoration: const InputDecoration(
                hintText: 'Enter name',
                labelText: 'Note',
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.people)),
            controller: nameController,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: BlocProvider.value(
              value: _categoryCubit,
              child: BlocBuilder<CategoryCubit, CategoryState>(
                builder: (context, state) {
                  if (state is InitialCategoryState ||
                      state is LoadingCategoryState) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (state is SuccessLoadAllCategoryState) {
                    final categories = state.listCategory;
                    final data = categories.data;
                    Set<String> list = {for (var v in data!) v[0].toString()};
                    return DropdownButtonFormField<String>(
                      hint: const Text("Select Category"),
                      value: selectedCategoryItem,
                      isExpanded: true,
                      icon: const Icon(Icons.arrow_drop_down),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                          labelText: 'Category',
                          prefixIcon: Icon(Icons.tag)
                      ),
                      onChanged: (String? value) {
                        // This is called when the user selects an item.
                        setState(() {
                          selectedCategoryItem = value;
                        });
                      },
                      items: list.map((categoryItem) {
                        return DropdownMenuItem<String>(
                          value: categoryItem,
                          child: Text(categoryItem),
                        );
                      }).toList(),
                    );
                  } else if (state is FailureCategoryState) {
                    return Center(child: Text(state.errorMessage));
                  }
                  return Text(state.toString());
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: BlocProvider.value(
              value: _priorityCubit,
              child: BlocBuilder<PriorityCubit, PriorityState>(
                builder: (context, state) {
                  if (state is InitialPriorityState ||
                      state is LoadingPriorityState) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (state is SuccessLoadAllPriorityState) {
                    final priorities = state.listPriority;
                    final data = priorities.data;
                    Set<String> list = {for (var v in data!) v[0].toString()};
                    return DropdownButtonFormField<String>(
                      hint: const Text("Select Priority"),
                      value: selectedPriorityItem,
                      isExpanded: true,
                      icon: const Icon(Icons.arrow_drop_down),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                          labelText: 'Priority',
                          prefixIcon: Icon(Icons.low_priority_sharp)
                      ),
                      onChanged: (String? value) {
                        setState(() {
                          selectedPriorityItem = value;
                        });
                      },
                      items: list.map((priorityItem) {
                        return DropdownMenuItem<String>(
                          value: priorityItem,
                          child: Text(priorityItem),
                        );
                      }).toList(),
                    );
                  } else if (state is FailurePriorityState) {
                    return Center(child: Text(state.errorMessage));
                  }
                  return Text(state.toString());
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: BlocProvider.value(
              value: _statusCubit,
              child: BlocBuilder<StatusCubit, StatusState>(
                builder: (context, state) {
                  if (state is InitialStatusState ||
                      state is LoadingStatusState) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (state is SuccessLoadAllStatusState) {
                    final status = state.listStatus;
                    final data = status.data;
                    Set<String> list = {for (var v in data!) v[0].toString()};
                    return DropdownButtonFormField<String>(
                      hint: const Text("Select Status"),
                      value: selectedStatusItem,
                      isExpanded: true,
                      icon: const Icon(Icons.arrow_drop_down),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Status',
                        prefixIcon: Icon(Icons.dehaze)
                      ),
                      onChanged: (String? value) {
                        // This is called when the user selects an item.
                        setState(() {
                          selectedStatusItem = value;
                        });
                      },
                      items: list.map((statusItem) {
                        return DropdownMenuItem<String>(
                          value: statusItem,
                          child: Text(statusItem),
                        );
                      }).toList(),
                    );
                  } else if (state is FailureStatusState) {
                    return Center(child: Text(state.errorMessage));
                  }
                  return Text(state.toString());
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: TextFormField(
              controller: _dateController,
              decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Date',
                  hintText: 'Pick a Date',
                  prefixIcon: InkWell(
                    onTap: () async {
                      DateTime? newDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime(2100),
                      );
                      if (newDate != null){
                        setState(() {
                          _date = newDate;
                          _dateController.text= '${_date.year}-${_date.month}-${_date.day}';
                        });
                      }
                    },
                    child: const Icon(Icons.calendar_today),
                  )
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return BlocBuilder<NoteCubit, NoteState>(builder: (_, state) {
      if (state is LoadingNoteState) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return Container();
      }
    });
  }
}
