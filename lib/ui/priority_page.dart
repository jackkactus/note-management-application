import 'package:noteapplication/respository/priority_repository.dart';
import 'package:noteapplication/ui/note_widget/note_import.dart';

class PriorityPage extends StatelessWidget {
  const PriorityPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key,});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final nameController = TextEditingController();
  final _priorityCubit = PriorityCubit(PriorityRepository());

  bool isUpdate = false;
  String oldName = '';
  String emailValue = '';
  int? statusCode = 0;
  int? statusError = 0;

  @override
  void initState(){
    super.initState();
    initValue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider.value(
        value: _priorityCubit,
        child: BlocBuilder<PriorityCubit, PriorityState>(
          builder: (context, state){
            if(state is InitialPriorityState || state is LoadingPriorityState){
              return const Center(child: CircularProgressIndicator());
            }
            else if(state is SuccessLoadAllPriorityState) {
              final priority = state.listPriority;
              statusCode = priority.status;
              final priorityData = priority.data;
              return ListView.builder(
                  itemCount: priorityData?.length,
                  itemBuilder: (context, index) {
                    final priority= priorityData?[index];
                    final color = Constance.lightColors[
                    index % Constance.lightColors.length];
                    return Card(
                      color: color,
                      margin: const EdgeInsets.all(15),
                      child: ListTile(
                        title: Text(priority[0]),
                        trailing: PopupMenuButton<int>(
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              value: 1,
                              child: IconButton(
                                onPressed: () {
                                  isUpdate = true;
                                  nameController.text = priority[0];
                                  oldName = priority[0];
                                  _showDialog(context, priority);
                                  _priorityCubit.getAllPriority(emailValue);
                                },
                                icon: const Icon(Icons.edit),
                              ),
                            ),
                            PopupMenuItem(
                                value: 2,
                                child: IconButton(
                                    onPressed: () async {
                                      await _priorityCubit.deletePriority(
                                          emailValue,
                                          priority[0]
                                      );
                                      if(statusCode==Constance.success){
                                        _priorityCubit.getAllPriority(emailValue);
                                      }
                                      else if(statusCode==Constance.failPriority && mounted){
                                        ScaffoldMessenger.of(context).showSnackBar(
                                            const SnackBar(
                                                content: Text('Priority Used')
                                            )
                                        );
                                      }
                                    },
                                    icon: const Icon(Icons.delete)
                                )
                            ),
                          ],
                        ),
                      ),
                    );
                  }
              );
            }
            else if(state is FailurePriorityState){
              return Center(child: Text(state.errorMessage),);
            }
            return Text(state.toString());
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            isUpdate = false;
            nameController.text = '';
            _showDialog(context, null);
            _priorityCubit.getAllPriority(emailValue);
          },
          label: const Text('Add Priority')
      ),
    );
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      emailValue = prefs.getString('getEmail') ?? "";
      _priorityCubit.getAllPriority(emailValue);
    });
  }

  Future<void> _showDialog (
      BuildContext context, List<dynamic>? priority) async{
    return showDialog(
        context: context,
        builder: (context){
          return StatefulBuilder(builder: (context, setState){
            return Dialog(
              insetPadding: EdgeInsets.zero,
              child: Scaffold(
                appBar: AppBar(
                  title: Text(isUpdate ? 'Update Priority' : 'Create new Priority'),
                  leading: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    icon: const Icon(Icons.close),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () async {
                          if (isUpdate == true) {
                            await _priorityCubit.updatePriority(emailValue, oldName, nameController.text);
                            if(statusCode==1){
                              _priorityCubit.getAllPriority(emailValue);
                            }
                            else if(statusCode==-1 && mounted){
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                  content: Text('Name is null. Please re-enter name.')
                              ));
                            }
                          }

                          if (isUpdate == false) {
                            await _priorityCubit.createPriority(emailValue, nameController.text);
                            if(statusCode==1){
                              _priorityCubit.getAllPriority(emailValue);
                            }
                            else if(statusCode==-1 && mounted){
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                  content: Text('Name is already exists.')
                              ));
                            }
                          }
                        },
                        child: const Text(
                          'Save',
                          style: TextStyle(color: Colors.white),
                        )
                    )
                  ],
                ),
                body: BlocProvider.value(
                  value: _priorityCubit,
                  child: BlocListener<PriorityCubit ,PriorityState>(
                    listener: (_, state){
                      if(state is SuccessSubmitPriorityState){
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text('Priority add successfully')
                        ));
                        setState(() {
                          nameController.clear();
                        });
                      }
                      else if(state is SuccessUpdatePriorityState){
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text('Priority update successfully')
                        ));
                      }
                      else if (state is FailurePriorityState){
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(state.errorMessage))
                        );
                      }
                    },
                    child: Stack(
                      children: [_buildFormWidget(), _buildLoadingWidget()],
                    ),
                  ),
                ),
              ),
            );
          });
        }
    );
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
  }

  Widget _buildFormWidget(){
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: const InputDecoration(
                hintText: 'Enter Priority',
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.people)
            ),
            controller: nameController,
          ),
        ),
      ],
    );
  }

  Widget _buildLoadingWidget(){
    return BlocBuilder<PriorityCubit, PriorityState>(builder: (_, state) {
      if(state is LoadingPriorityState) {
        return const Center(child: CircularProgressIndicator(),);
      } else {
        return Container();
      }
    });
  }
}

