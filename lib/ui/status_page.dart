import 'package:noteapplication/cubit/status_cubit.dart';
import 'package:noteapplication/respository/status_repository.dart';
import 'package:noteapplication/state/status_state.dart';
import 'package:noteapplication/ui/note_widget/note_import.dart';

class StatusPage extends StatelessWidget {
  const StatusPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({
    super.key,
  });

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final nameController = TextEditingController();
  final _statusCubit = StatusCubit(StatusRepository());

  bool isUpdate = false;
  String oldName = '';
  String emailValue = '';
  int? statusCode = 0;
  int? statusError = 0;

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefEmail = prefs.getString(Constance.getEmail) ?? "";
    setState(() {
      emailValue = prefEmail;
      _statusCubit.getAllStatus(emailValue);
    });
  }

  @override
  void initState() {
    super.initState();
    initValue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider.value(
        value: _statusCubit,
        child: BlocBuilder<StatusCubit, StatusState>(
          builder: (context, state) {
            if (state is InitialStatusState || state is LoadingStatusState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is SuccessLoadAllStatusState) {
              final status = state.listStatus;
              statusCode = status.status;
              final statusData = status.data;
              return ListView.builder(
                  itemCount: statusData?.length,
                  itemBuilder: (context, index) {
                    final status = statusData?[index];
                    final color = Constance
                        .lightColors[index % Constance.lightColors.length];
                    return Card(
                      color: color,
                      margin: const EdgeInsets.all(15),
                      child: ListTile(
                        title: Text(status[0]),
                        trailing: PopupMenuButton<int>(
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              value: 1,
                              child: IconButton(
                                onPressed: () {
                                  isUpdate = true;
                                  nameController.text = status[0];
                                  oldName = status[0];
                                  _showDialog(context, status);
                                  _statusCubit.getAllStatus(emailValue);
                                },
                                icon: const Icon(Icons.edit),
                              ),
                            ),
                            PopupMenuItem(
                                value: 2,
                                child: IconButton(
                                    onPressed: () async {
                                      await _statusCubit.deleteStatus(
                                          emailValue, status[0]);
                                      if (statusCode == Constance.success) {
                                        _statusCubit
                                            .getAllStatus(emailValue);
                                      } else if (statusCode == Constance.failStatus && mounted) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(const SnackBar(
                                                content: Text('Status Used')));
                                      }
                                    },
                                    icon: const Icon(Icons.delete))),
                          ],
                        ),
                      ),
                    );
                  });
            } else if (state is FailureStatusState) {
              return Center(
                child: Text(state.errorMessage),
              );
            }
            return Text(state.toString());
          },
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            isUpdate = false;
            nameController.text = '';
            _showDialog(context, null);
            _statusCubit.getAllStatus(emailValue);
          },
          label: const Text('Add Status')),
    );
  }
  

  Future<void> _showDialog(BuildContext context, List<dynamic>? status) async {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              insetPadding: EdgeInsets.zero,
              child: Scaffold(
                appBar: AppBar(
                  title: Text(isUpdate ? 'Update Status' : 'Create new Status'),
                  leading: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    icon: const Icon(Icons.close),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () async {
                          if (isUpdate == true) {
                            await _statusCubit.updateStatus(emailValue,
                                oldName, nameController.text);
                            if (statusCode == 1) {
                              _statusCubit.getAllStatus(emailValue);
                            } else if (statusCode == -1 && mounted) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text(
                                          'Name is null. Please re-enter name.')));
                            }
                          }

                          if (isUpdate == false) {
                            await _statusCubit.createStatus(
                                emailValue, nameController.text);
                            if (statusCode == 1) {
                              _statusCubit.getAllStatus(emailValue);
                            } else if (statusCode == -1 && mounted) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content:
                                          Text('Name is already exists.')));
                            }
                          }
                        },
                        child: const Text(
                          'Save',
                          style: TextStyle(color: Colors.white),
                        ))
                  ],
                ),
                body: BlocProvider.value(
                  value: _statusCubit,
                  child: BlocListener<StatusCubit, StatusState>(
                    listener: (_, state) {
                      if (state is SuccessSubmitStatusState) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Status add successfully')));
                        setState(() {
                          nameController.clear();
                        });
                      } else if (state is SuccessUpdateStatusState) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Status update successfully')));
                      } else if (state is FailureStatusState) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(state.errorMessage)));
                      }
                    },
                    child: Stack(
                      children: [_buildFormWidget(), _buildLoadingWidget()],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
  }

  Widget _buildFormWidget() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: const InputDecoration(
                hintText: 'Enter status',
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.people)),
            controller: nameController,
          ),
        ),
      ],
    );
  }

  Widget _buildLoadingWidget() {
    return BlocBuilder<StatusCubit, StatusState>(builder: (_, state) {
      if (state is LoadingStatusState) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return Container();
      }
    });
  }
}
