import 'package:noteapplication/ui/note_widget/note_import.dart';

class Constance {
  static const String baseUrl = 'https://it-fresher.edu.vn/nms/';
  static const int statusCode200 = 200;
  static const String remember = "remember";
  static const String email ="email";
  static const String password="password";
  static const String getEmail="getEmail";
  static const String getPassword="getPassword";
  static const String getName = "getName";

  static const int pageHome = 1;
  static const int pageCategory = 2;
  static const int pagePriority = 3;
  static const int pageStatus = 4;
  static const int pageNote = 5;
  static const int pageEditProfile = 6;
  static const int pageChangePassword = 7;
  static const int pageLogout = 8;

  static const int success = 1;
  static const int failChangePassword = -1;
  static const int fail = -1;
  static const int failPassword = 2;
  static const int emailExist = 2;
  static const int failCategory = -1;
  static const int failNote = -1;
  static const int failPriority = -1;
  static const int failStatus = -1;


  static final lightColors = [
    Colors.amber.shade300,
    Colors.lightGreen.shade300,
    Colors.lightBlue.shade300,
    Colors.orange.shade300,
    Colors.pinkAccent.shade100,
    Colors.tealAccent.shade100
  ];
}
